"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ava_1 = __importDefault(require("ava"));
const lodash_1 = require("lodash");
// import { Client } from './client';
const proxyquire_1 = __importDefault(require("proxyquire"));
const sinon_1 = __importDefault(require("sinon"));
const pathStub = sinon_1.default.stub().callsFake((...a) => ({
    args: [...a]
}));
const c = proxyquire_1.default('./client', {
    child_process: {
        spawn: pathStub
    }
});
const Client = c.Client;
// Global constants
const BIN_PATH = '/Users';
ava_1.default('can create client with no config', t => {
    const client = new Client({});
    const f = client.launch(BIN_PATH);
    const a = f.args;
    const path = a[0];
    const flags = a[1];
    // This should match daemon path passed into the function
    t.is(path, BIN_PATH);
    // Check that default flags are passed in
    const hasApiAddr = flags.includes('--api-addr=localhost:4280');
    const hasHostAddr = flags.includes('--host-addr=:4282');
    const hasRpcAddr = flags.includes('--rpc-addr=:4281');
    t.true(hasApiAddr);
    t.true(hasHostAddr);
    t.true(hasRpcAddr);
});
ava_1.default('can replace client config', t => {
    const client = new Client({
        agent: 'custom-agent',
        apiAuthentication: true,
        apiAuthenticationPassword: 'foo',
        apiHost: '1.1.1.1',
        apiPort: 1337,
        dataDirectory: 'bar',
        hostPort: 1339,
        modules: {
            consensus: true,
            explorer: true,
            gateway: true,
            host: true,
            miner: false,
            renter: true,
            transactionPool: true,
            wallet: true
        },
        rpcPort: 1338
    });
    const f = client.launch(BIN_PATH);
    const a = f.args[1];
    t.is(a.length, 7);
    t.true(lodash_1.some(a, x => x.includes('--agent=custom-agent')));
    t.true(lodash_1.some(a, x => x.includes('--api-addr=1.1.1.1:1337')));
    t.true(lodash_1.some(a, x => x.includes('--authenticate-api=true')));
    t.true(lodash_1.some(a, x => x.includes('--host-addr=:1339')));
    // skipped module test because we have a seperate unit test for parseModules
    t.true(lodash_1.some(a, x => x.includes('--rpc-addr=:1338')));
    t.true(lodash_1.some(a, x => x.includes('--siaprime-directory=bar')));
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LnNwZWMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvbGliL2NsaWVudC5zcGVjLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsOENBQXVCO0FBQ3ZCLG1DQUE4QjtBQUM5QixxQ0FBcUM7QUFDckMsNERBQW9DO0FBQ3BDLGtEQUEwQjtBQUUxQixNQUFNLFFBQVEsR0FBRyxlQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDakQsSUFBSSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUM7Q0FDYixDQUFDLENBQUMsQ0FBQztBQUNKLE1BQU0sQ0FBQyxHQUFHLG9CQUFVLENBQUMsVUFBVSxFQUFFO0lBQy9CLGFBQWEsRUFBRTtRQUNiLEtBQUssRUFBRSxRQUFRO0tBQ2hCO0NBQ0YsQ0FBQyxDQUFDO0FBQ0gsTUFBTSxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztBQUV4QixtQkFBbUI7QUFDbkIsTUFBTSxRQUFRLEdBQUcsUUFBUSxDQUFDO0FBRTFCLGFBQUksQ0FBQyxrQ0FBa0MsRUFBRSxDQUFDLENBQUMsRUFBRTtJQUMzQyxNQUFNLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUM5QixNQUFNLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ2xDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDakIsTUFBTSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ2xCLE1BQU0sS0FBSyxHQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM3Qix5REFBeUQ7SUFDekQsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFFckIseUNBQXlDO0lBQ3pDLE1BQU0sVUFBVSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsMkJBQTJCLENBQUMsQ0FBQztJQUMvRCxNQUFNLFdBQVcsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUM7SUFDeEQsTUFBTSxVQUFVLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO0lBQ3RELENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDbkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNwQixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0FBQ3JCLENBQUMsQ0FBQyxDQUFDO0FBRUgsYUFBSSxDQUFDLDJCQUEyQixFQUFFLENBQUMsQ0FBQyxFQUFFO0lBQ3BDLE1BQU0sTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDO1FBQ3hCLEtBQUssRUFBRSxjQUFjO1FBQ3JCLGlCQUFpQixFQUFFLElBQUk7UUFDdkIseUJBQXlCLEVBQUUsS0FBSztRQUNoQyxPQUFPLEVBQUUsU0FBUztRQUNsQixPQUFPLEVBQUUsSUFBSTtRQUNiLGFBQWEsRUFBRSxLQUFLO1FBQ3BCLFFBQVEsRUFBRSxJQUFJO1FBQ2QsT0FBTyxFQUFFO1lBQ1AsU0FBUyxFQUFFLElBQUk7WUFDZixRQUFRLEVBQUUsSUFBSTtZQUNkLE9BQU8sRUFBRSxJQUFJO1lBQ2IsSUFBSSxFQUFFLElBQUk7WUFDVixLQUFLLEVBQUUsS0FBSztZQUNaLE1BQU0sRUFBRSxJQUFJO1lBQ1osZUFBZSxFQUFFLElBQUk7WUFDckIsTUFBTSxFQUFFLElBQUk7U0FDYjtRQUNELE9BQU8sRUFBRSxJQUFJO0tBQ2QsQ0FBQyxDQUFDO0lBQ0gsTUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNsQyxNQUFNLENBQUMsR0FBYSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBRTlCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNsQixDQUFDLENBQUMsSUFBSSxDQUFDLGFBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3pELENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDNUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM1RCxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3RELDRFQUE0RTtJQUM1RSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3JELENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDL0QsQ0FBQyxDQUFDLENBQUMifQ==