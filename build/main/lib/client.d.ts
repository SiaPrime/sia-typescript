/// <reference types="node" />
import { ChildProcess } from 'child_process';
import http from 'http';
import rp from 'request-promise-native';
import { Url } from 'url';
import { ClientConfig } from './proto';
export declare class Client {
    protected config: ClientConfig;
    protected process: ChildProcess;
    protected agent: http.Agent;
    constructor(config?: ClientConfig);
    launch: (binPath: string) => ChildProcess;
    makeRequest: (endpoint: string | Url, querystring?: object, method?: string, timeout?: number) => Promise<any>;
    call: (options: string | rp.OptionsWithUrl) => Promise<any>;
    gateway: () => Promise<any>;
    daemonVersion: () => Promise<any>;
    daemonStop: () => Promise<any>;
    /**
     * checks if siad responds to a /version call.
     */
    isRunning: () => Promise<boolean>;
    getConnectionUrl: () => string;
    private mergeDefaultRequestOptions;
}
