import fs from 'fs';
import os from 'os';
import path from 'path';
/**
 * Works similarly to Object.assign, but checks properties for undefined or
 * null values, skipping them if detected.
 * @param target
 * @param sources
 */
export function assignDefined(target, ...sources) {
    for (const source of sources) {
        for (const key of Object.keys(source)) {
            const val = source[key];
            if (val !== undefined && val !== null) {
                target[key] = val;
            }
        }
    }
    return target;
}
/**
 * Retrieves the API password using the SIA_API_PASSWORD env variable or
 * attempt to read the local dir with fs.
 */
export function getSiaPassword() {
    try {
        let configPath;
        switch (process.platform) {
            case 'win32':
                configPath = path.join(process.env.LOCALAPPDATA, 'SiaPrime');
                break;
            case 'darwin':
                configPath = path.join(os.homedir(), 'Library', 'Application Support', 'SiaPrime');
                break;
            default:
                configPath = path.join(os.homedir(), '.siaprime');
        }
        const password = process.env.SIA_API_PASSWORD
            ? process.env.SIA_API_PASSWORD
            : fs.readFileSync(path.join(configPath, 'apipassword')).toString();
        return password.trim() || '';
    }
    catch (err) {
        // if apipassword doesn't exist, we'll just return an emtpy string
        return '';
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvbGliL3V0aWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxNQUFNLElBQUksQ0FBQztBQUNwQixPQUFPLEVBQUUsTUFBTSxJQUFJLENBQUM7QUFDcEIsT0FBTyxJQUFJLE1BQU0sTUFBTSxDQUFDO0FBRXhCOzs7OztHQUtHO0FBQ0gsTUFBTSxVQUFVLGFBQWEsQ0FBQyxNQUFjLEVBQUUsR0FBRyxPQUFpQjtJQUNoRSxLQUFLLE1BQU0sTUFBTSxJQUFJLE9BQU8sRUFBRTtRQUM1QixLQUFLLE1BQU0sR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDckMsTUFBTSxHQUFHLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3hCLElBQUksR0FBRyxLQUFLLFNBQVMsSUFBSSxHQUFHLEtBQUssSUFBSSxFQUFFO2dCQUNyQyxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDO2FBQ25CO1NBQ0Y7S0FDRjtJQUNELE9BQU8sTUFBTSxDQUFDO0FBQ2hCLENBQUM7QUFFRDs7O0dBR0c7QUFDSCxNQUFNLFVBQVUsY0FBYztJQUM1QixJQUFJO1FBQ0YsSUFBSSxVQUFVLENBQUM7UUFDZixRQUFRLE9BQU8sQ0FBQyxRQUFRLEVBQUU7WUFDeEIsS0FBSyxPQUFPO2dCQUNWLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBc0IsRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFDdkUsTUFBTTtZQUNSLEtBQUssUUFBUTtnQkFDWCxVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FDcEIsRUFBRSxDQUFDLE9BQU8sRUFBRSxFQUNaLFNBQVMsRUFDVCxxQkFBcUIsRUFDckIsVUFBVSxDQUNYLENBQUM7Z0JBQ0YsTUFBTTtZQUNSO2dCQUNFLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBRSxXQUFXLENBQUMsQ0FBQztTQUNyRDtRQUNELE1BQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCO1lBQzNDLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQjtZQUM5QixDQUFDLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxhQUFhLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3JFLE9BQU8sUUFBUSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQztLQUM5QjtJQUFDLE9BQU8sR0FBRyxFQUFFO1FBQ1osa0VBQWtFO1FBQ2xFLE9BQU8sRUFBRSxDQUFDO0tBQ1g7QUFDSCxDQUFDIn0=