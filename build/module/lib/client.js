var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { spawn } from 'child_process';
import fs from 'fs';
import http from 'http';
import rp from 'request-promise-native';
import { parseFlags } from './flags';
import { getSiaPassword } from './utils';
export class Client {
    constructor(config = {}) {
        this.launch = (binPath) => {
            try {
                // Check if siad exists
                if (fs.existsSync(binPath)) {
                    // Create flags
                    const flags = parseFlags(this.config);
                    // Set euid if avl
                    const opts = {};
                    if (process.geteuid) {
                        opts.uid = process.geteuid();
                    }
                    this.process = spawn(binPath, flags, opts);
                    return this.process;
                }
                else {
                    throw new Error('could not find binary file in filesystem');
                }
            }
            catch (e) {
                throw new Error(e);
            }
        };
        this.makeRequest = (endpoint, querystring, method = 'GET', timeout = 30000) => __awaiter(this, void 0, void 0, function* () {
            try {
                const requestOptions = this.mergeDefaultRequestOptions({
                    url: endpoint,
                    timeout,
                    qs: querystring,
                    method
                });
                const data = yield rp(requestOptions);
                return data;
            }
            catch (e) {
                throw e;
            }
        });
        this.call = (options) => {
            if (typeof options === 'string') {
                return this.makeRequest(options);
            }
            else {
                const endpoint = options.url;
                const method = options.method;
                const qs = options.qs || undefined;
                const timeout = options.timeout || undefined;
                return this.makeRequest(endpoint, qs, method, timeout);
            }
        };
        this.gateway = () => {
            return this.makeRequest('/gateway');
        };
        this.daemonVersion = () => {
            return this.makeRequest('/daemon/version');
        };
        this.daemonStop = () => {
            return this.makeRequest('/daemon/stop');
        };
        /**
         * checks if siad responds to a /version call.
         */
        this.isRunning = () => __awaiter(this, void 0, void 0, function* () {
            if (this.process) {
                try {
                    yield this.daemonVersion();
                    return true;
                }
                catch (e) {
                    return false;
                }
            }
            else {
                try {
                    yield this.daemonVersion();
                    return true;
                }
                catch (e) {
                    return false;
                }
            }
        });
        this.getConnectionUrl = () => {
            if (!this.config.apiAuthenticationPassword) {
                this.config.apiAuthenticationPassword = getSiaPassword();
            }
            return `http://:${this.config.apiAuthenticationPassword}@${this.config.apiHost}:${this.config.apiPort}`;
        };
        this.mergeDefaultRequestOptions = (opts) => {
            // These are the default config sourced from the Sia Agent
            const defaultOptions = {
                baseUrl: this.getConnectionUrl(),
                headers: {
                    'User-Agent': this.config.agent || 'SiaPrime-Agent'
                },
                json: true,
                pool: this.agent,
                timeout: 30000
            };
            const formattedOptions = Object.assign({}, defaultOptions, opts);
            return formattedOptions;
        };
        try {
            if (config.dataDirectory) {
                fs.existsSync(config.dataDirectory);
            }
            const defaultConfig = {
                apiAuthentication: 'auto',
                apiHost: 'localhost',
                apiPort: 4280,
                hostPort: 4282,
                rpcPort: 4281
            };
            this.config = Object.assign({}, defaultConfig, config);
            // If strategy is set to 'auto', attempt to read from default siapassword file.
            if (this.config.apiAuthentication === 'auto') {
                this.config.apiAuthenticationPassword = getSiaPassword();
            }
            this.agent = new http.Agent({
                keepAlive: true,
                maxSockets: 30
            });
        }
        catch (e) {
            throw new Error(e);
        }
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9jbGllbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxPQUFPLEVBQWdCLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEVBQUUsTUFBTSxJQUFJLENBQUM7QUFDcEIsT0FBTyxJQUFJLE1BQU0sTUFBTSxDQUFDO0FBRXhCLE9BQU8sRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBRXhDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFFckMsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLFNBQVMsQ0FBQztBQUV6QyxNQUFNLE9BQU8sTUFBTTtJQVFqQixZQUFZLFNBQXVCLEVBQUU7UUEwQjlCLFdBQU0sR0FBRyxDQUFDLE9BQWUsRUFBZ0IsRUFBRTtZQUNoRCxJQUFJO2dCQUNGLHVCQUF1QjtnQkFDdkIsSUFBSSxFQUFFLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUMxQixlQUFlO29CQUNmLE1BQU0sS0FBSyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3RDLGtCQUFrQjtvQkFDbEIsTUFBTSxJQUFJLEdBQVEsRUFBRSxDQUFDO29CQUNyQixJQUFJLE9BQU8sQ0FBQyxPQUFPLEVBQUU7d0JBQ25CLElBQUksQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO3FCQUM5QjtvQkFFRCxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUUzQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7aUJBQ3JCO3FCQUFNO29CQUNMLE1BQU0sSUFBSSxLQUFLLENBQUMsMENBQTBDLENBQUMsQ0FBQztpQkFDN0Q7YUFDRjtZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNWLE1BQU0sSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDcEI7UUFDSCxDQUFDLENBQUM7UUFFSyxnQkFBVyxHQUFHLENBQ25CLFFBQXNCLEVBQ3RCLFdBQWdDLEVBQ2hDLFNBQWlCLEtBQUssRUFDdEIsVUFBa0IsS0FBSyxFQUN2QixFQUFFO1lBQ0YsSUFBSTtnQkFDRixNQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUM7b0JBQ3JELEdBQUcsRUFBRSxRQUFRO29CQUNiLE9BQU87b0JBQ1AsRUFBRSxFQUFFLFdBQVc7b0JBQ2YsTUFBTTtpQkFDUCxDQUFDLENBQUM7Z0JBQ0gsTUFBTSxJQUFJLEdBQUcsTUFBTSxFQUFFLENBQUMsY0FBYyxDQUFDLENBQUM7Z0JBQ3RDLE9BQU8sSUFBSSxDQUFDO2FBQ2I7WUFBQyxPQUFPLENBQUMsRUFBRTtnQkFDVixNQUFNLENBQUMsQ0FBQzthQUNUO1FBQ0gsQ0FBQyxDQUFBLENBQUM7UUFFSyxTQUFJLEdBQUcsQ0FBQyxPQUFtQyxFQUFFLEVBQUU7WUFDcEQsSUFBSSxPQUFPLE9BQU8sS0FBSyxRQUFRLEVBQUU7Z0JBQy9CLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNsQztpQkFBTTtnQkFDTCxNQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDO2dCQUM3QixNQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDO2dCQUM5QixNQUFNLEVBQUUsR0FBRyxPQUFPLENBQUMsRUFBRSxJQUFJLFNBQVMsQ0FBQztnQkFDbkMsTUFBTSxPQUFPLEdBQUcsT0FBTyxDQUFDLE9BQU8sSUFBSSxTQUFTLENBQUM7Z0JBQzdDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQzthQUN4RDtRQUNILENBQUMsQ0FBQztRQUVLLFlBQU8sR0FBRyxHQUFHLEVBQUU7WUFDcEIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3RDLENBQUMsQ0FBQztRQUVLLGtCQUFhLEdBQUcsR0FBRyxFQUFFO1lBQzFCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQzdDLENBQUMsQ0FBQztRQUVLLGVBQVUsR0FBRyxHQUFHLEVBQUU7WUFDdkIsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQzFDLENBQUMsQ0FBQztRQUVGOztXQUVHO1FBQ0ksY0FBUyxHQUFHLEdBQTJCLEVBQUU7WUFDOUMsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNoQixJQUFJO29CQUNGLE1BQU0sSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO29CQUMzQixPQUFPLElBQUksQ0FBQztpQkFDYjtnQkFBQyxPQUFPLENBQUMsRUFBRTtvQkFDVixPQUFPLEtBQUssQ0FBQztpQkFDZDthQUNGO2lCQUFNO2dCQUNMLElBQUk7b0JBQ0YsTUFBTSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7b0JBQzNCLE9BQU8sSUFBSSxDQUFDO2lCQUNiO2dCQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNWLE9BQU8sS0FBSyxDQUFDO2lCQUNkO2FBQ0Y7UUFDSCxDQUFDLENBQUEsQ0FBQztRQUVLLHFCQUFnQixHQUFHLEdBQVcsRUFBRTtZQUNyQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsRUFBRTtnQkFDMUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsR0FBRyxjQUFjLEVBQUUsQ0FBQzthQUMxRDtZQUNELE9BQU8sV0FBVyxJQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixJQUNyRCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQ2QsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzVCLENBQUMsQ0FBQztRQUVNLCtCQUEwQixHQUFHLENBQ25DLElBQXVCLEVBQ0osRUFBRTtZQUNyQiwwREFBMEQ7WUFDMUQsTUFBTSxjQUFjLEdBQXdCO2dCQUMxQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFO2dCQUNoQyxPQUFPLEVBQUU7b0JBQ1AsWUFBWSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxJQUFJLGdCQUFnQjtpQkFDcEQ7Z0JBQ0QsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLO2dCQUNoQixPQUFPLEVBQUUsS0FBSzthQUNmLENBQUM7WUFDRixNQUFNLGdCQUFnQixxQkFBUSxjQUFjLEVBQUssSUFBSSxDQUFFLENBQUM7WUFDeEQsT0FBTyxnQkFBZ0IsQ0FBQztRQUMxQixDQUFDLENBQUM7UUF6SUEsSUFBSTtZQUNGLElBQUksTUFBTSxDQUFDLGFBQWEsRUFBRTtnQkFDeEIsRUFBRSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7YUFDckM7WUFDRCxNQUFNLGFBQWEsR0FBaUI7Z0JBQ2xDLGlCQUFpQixFQUFFLE1BQU07Z0JBQ3pCLE9BQU8sRUFBRSxXQUFXO2dCQUNwQixPQUFPLEVBQUUsSUFBSTtnQkFDYixRQUFRLEVBQUUsSUFBSTtnQkFDZCxPQUFPLEVBQUUsSUFBSTthQUNkLENBQUM7WUFDRixJQUFJLENBQUMsTUFBTSxxQkFBUSxhQUFhLEVBQUssTUFBTSxDQUFFLENBQUM7WUFDOUMsK0VBQStFO1lBQy9FLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsS0FBSyxNQUFNLEVBQUU7Z0JBQzVDLElBQUksQ0FBQyxNQUFNLENBQUMseUJBQXlCLEdBQUcsY0FBYyxFQUFFLENBQUM7YUFDMUQ7WUFDRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQztnQkFDMUIsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsVUFBVSxFQUFFLEVBQUU7YUFDZixDQUFDLENBQUM7U0FDSjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1YsTUFBTSxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNwQjtJQUNILENBQUM7Q0FtSEYifQ==