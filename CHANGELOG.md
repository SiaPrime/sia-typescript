# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.2.3"></a>
## [1.2.3](https://gitlab.com/NebulousLabs/sia-typescript/compare/v1.2.2...v1.2.3) (2019-03-31)



<a name="1.2.2"></a>
## [1.2.2](https://gitlab.com/NebulousLabs/sia-typescript/compare/v1.2.1...v1.2.2) (2019-03-30)



<a name="1.2.1"></a>
## [1.2.1](https://gitlab.com/NebulousLabs/sia-typescript/compare/v1.2.0...v1.2.1) (2019-03-10)


### Bug Fixes

* **apipassword:** fix auto strategy in getConnectionUrl ([0b550c4](https://gitlab.com/NebulousLabs/sia-typescript/commit/0b550c4))



<a name="1.2.0"></a>
# [1.2.0](https://gitlab.com/NebulousLabs/sia-typescript/compare/v1.1.3...v1.2.0) (2019-03-08)


### Features

* **unit tests:** use proxyquire and remove spawn in constructor ([0581ba9](https://gitlab.com/NebulousLabs/sia-typescript/commit/0581ba9))



<a name="1.1.3"></a>
## [1.1.3](https://gitlab.com/NebulousLabs/sia-typescript/compare/v1.1.1...v1.1.3) (2019-02-25)


### Bug Fixes

* **apipassword:** fix apipassword auto strategy error ([d6231ab](https://gitlab.com/NebulousLabs/sia-typescript/commit/d6231ab))
* **utils:** add more descriptive error logging for getSiaPassword ([6b98788](https://gitlab.com/NebulousLabs/sia-typescript/commit/6b98788))



<a name="1.1.1"></a>
## [1.1.1](https://github.com/eddiewang/siajs-lib/compare/v1.1.0...v1.1.1) (2019-02-07)


### Bug Fixes

* change to es2015 module ([626e357](https://github.com/eddiewang/siajs-lib/commit/626e357))



<a name="1.1.0"></a>
# 1.1.0 (2019-02-07)


### Features

* initial set of features to use ([441d27b](https://github.com/eddiewang/siajs-lib/commit/441d27b))
